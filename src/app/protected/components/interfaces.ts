import {supplierIdGetter} from "../../app.module";

export interface IIngredient {
  name: string;
  origin: string;
  possibleAllergies: string[];
}

export class Ingredient {
  name: string | undefined;
  origin: string | undefined;
  possibleAllergies: string[] = [];

  constructor() {
    this.name = '';
    this.origin ='';
    this.possibleAllergies = [];
  }
}

export class Product {
  id: string | undefined;
  name: string | undefined;
  description: string | undefined;
  price: number | undefined;
  ingredients: Ingredient[] = []
  isVegetarian: boolean | undefined;
  isVegan: boolean | undefined;
  category: Category = new Category();
  measurement: Measurement = new Measurement();
  weight: number | undefined;
  supplier: Supplier | undefined = this.getSupplier();
  countryOfOrigin: Country = new Country();
  countriesSell: Country[] = [new Country()];

  constructor() {
    this.name = '';
    this.description = '';
    this.price = 0;
    this.ingredients = [new Ingredient()]
  }

  getSupplier() {
    return SupplierList.find(supplier=> supplier.id == supplierIdGetter());
  }
}
export class Supplier {
  id: string | undefined;
  name: string | undefined;
  email: string | undefined;
}

export class Category {
  id: string | undefined;
  name: string | undefined;
  rayon: string | undefined;
}

export class Country {
  name: string | undefined;
  id: string | undefined;
  languages: string[] = [];
  continent: string | undefined;
}

export class Measurement {
  height: number | undefined;
  width: number | undefined
  length: number | undefined;
}


export const CountryList: Country[] = [
  {
    name: 'France',
    id: 'fr',
    languages: ['LANG_FR'],
    continent: 'EU'
  },
  {
    name: 'Italy',
    id: 'it',
    languages: ['LANG_IT'],
    continent: 'EU'
  },
  {
    name: 'United States',
    id: 'us',
    languages: ['LANG_EN'],
    continent: 'AM'
  },
  {
    name: 'Lebanon',
    id: 'lb',
    languages: ['LANG_AR'],
    continent: 'AS'
  }
];

export const SupplierList: Supplier[] = [
  {
    id: '10000',
    name: 'LVMH',
    email: 'nohra.rosette@gmail.com'
  },
  {
    id: '10001',
    name: 'PepsiCo',
    email: 'nohra.rosette@gmail.com'
  }
];

export const CategoryList: Category[] = [
  {
    id: 'Cat1',
    name: 'Food',
    rayon: 'R1'
  },
  {
    id: 'Cat2',
    name: 'Beverages',
    rayon: 'R2'
  },
  {
    id: 'Cat3',
    name: 'Frozen Food',
    rayon: 'R3'
  },
  {
    id: 'Cat4',
    name: 'Condiments',
    rayon: 'R4'
  },
]

export class Report {
  id: String | undefined;
  type: String | undefined;
  desc: String | undefined;
  productIdsSelected: string[] | undefined;
}
