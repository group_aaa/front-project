import { Component, OnInit } from '@angular/core';
import {Product, Report} from "../interfaces";
import {ServiceService} from "../service.service";

@Component({
  selector: 'app-reports',
  templateUrl: './reports.component.html',
  styleUrls: ['./reports.component.css']
})
export class ReportsComponent implements OnInit {

  productIdsSelected: string[] = []
  reports: Report[] = [
    {
      id: 'COMPLETENESS_REPORT',
      type: 'COMPLETION REPORT',
      desc: 'calculates the percentage of completion of the products',
      productIdsSelected: []
    },
    {
      id: 'DATA_EXPORT',
      type: 'EXPORT PRODUCTS REPORT',
      desc: 'get the data of your products',
      productIdsSelected: []
    },
    {
      id: 'PRODUCT_LIFECYCLE',
      type: 'PRODUCTS LIFECYCLE',
      desc: 'check the history of all your changes for your products',
      productIdsSelected: []
    }

  ];
  constructor(private serviceService: ServiceService) { }

  ngOnInit(): void {

  }

  generateReport(report: Report) {
    this.serviceService.generateReport(report).subscribe();
  }

}
