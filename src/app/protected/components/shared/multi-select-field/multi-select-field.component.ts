import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';

@Component({
  selector: 'app-multi-select-field',
  templateUrl: './multi-select-field.component.html',
  styleUrls: ['./multi-select-field.component.css']
})
export class MultiSelectFieldComponent implements OnInit {

  constructor() { }

  @Input() options: any;
  @Input() isMulti = false;
  @Input() type: string = '';

  @Input() selectedOptions: any = [];
  @Input() selectedOption: any = {};
  @Output() selectedOptionsChange: EventEmitter<any> = new EventEmitter<any>();
  @Output() selectedOptionChange: EventEmitter<any> = new EventEmitter<any>();
  checkedOptionsIds: any = [];

  ngOnInit(): void {
    this.selectedOptions = [];
    this.selectedOption = null;
  }

  selectUnselectOption(option: any) {
    if (!this.isMulti && this.selectedOption && this.selectedOption.id == option.id) {
      this.checkedOptionsIds[option.id] = false;
      this.selectedOption = null;
    } else if (!this.isMulti) {
      this.checkedOptionsIds[option.id] = true;
      this.selectedOption = option;
    }
    else if (this.isMulti && this.selectedOptions && this.selectedOptions.length > 0) {
      let index = this.selectedOptions.findIndex((opt: any) => opt.id == option.id)
      if (index >= 0) {
        this.checkedOptionsIds[option.id] = false;
        this.selectedOptions.splice(index,1);
      }
      else  {
        this.checkedOptionsIds[option.id] = true;
        this.selectedOptions.push(option);
      }
    }
    else {
      this.checkedOptionsIds[option.id] = true;
      this.selectedOptions.push(option);
    }

    this.selectedOptionChange.emit(this.selectedOption);
    this.selectedOptionsChange.emit(this.selectedOptions);

  }

}
