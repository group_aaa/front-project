import {Component, EventEmitter, Input, OnInit, Output, ViewChild} from '@angular/core';

@Component({
  selector: 'app-multi-value-field',
  templateUrl: './multi-value-field.component.html',
  styleUrls: ['./multi-value-field.component.css']
})
export class MultiValueFieldComponent implements OnInit {

  @Input() values: any;
  @Input() placeholder = 'Add value'
  @Output() valuesChange = new EventEmitter();
  @Output() submitted = new EventEmitter();
  @Output() cleared = new EventEmitter()
  @Output() valueRemoved = new EventEmitter()

  @Input() type: any

  @ViewChild('newValueField',{static: true}) newValueField: any;
  @ViewChild('uploadFileElem',{static: false}) uploadFileElem: any;
  selectedValueIndex: any;
  newValue: any;

  constructor() {

  }
  ngOnInit() {
    if (typeof this.values == 'undefined' || this.values == null)
      this.values = [];
  }

  checkText(){
    if(this.newValue)
      this.newValue = this.checkWord(this.newValue)
  }


  addValue(focusOnField: any){

    if(typeof focusOnField == 'undefined' || focusOnField == null) focusOnField=true;

    this.addValueDirect(this.newValue,0);
    this.newValue = null;
    if(focusOnField)
      this.newValueField.nativeElement.focus();
    this.valuesChange.emit(this.values);
  }

  //If pasted text contains line carriages, add values directly to list
  pastedText(event: any){

    var txt=event.clipboardData.getData('text/plain');
    if(txt.includes("\r")){

      var lines=txt.split('\r');
      for(var i=0;i<lines.length;i++){

        this.addValueDirect(lines[i],this.values.length+i);
      }

      var inst=this;
      setTimeout(function(){

        inst.newValue=null;
      },100);
    }
  }

  addValueDirect(val: any, index: any){

    if (val == null || val.trim() == '') return;

    //handle comma separated values
    if(val.includes(',')){

      var arr=val.split(',');
      for(var i=0;i<arr.length;i++){

        this.addValueDirect(arr[i],0);
      }
      return;
    }

    val=val.trim();

    if (index == 0)
      this.values.unshift(val);
    else {

      this.values.splice(index, 0, val);
    }
  }

  uploadFile() {

    this.uploadFileElem.nativeElement.click();
  }

  uploadFileListener(event: any) {

    var reader: FileReader = new FileReader();
    var inst = this;
    reader.onloadend = function (e) {
      var lines = (<string>reader.result).split("\n")
      for (var i = 0; i < lines.length; i++) {

        inst.addValueDirect(lines[i], i);
      }
    }
    reader.readAsText(event.target.files[0], 'UTF-8');
    this.uploadFileElem.nativeElement.value = "";
  }

  removeSelectedValue() {

    this.values.splice(this.selectedValueIndex, 1);
    this.valueRemoved.emit(this.values)
  }

  removeAllValues() {

    this.values.length = 0;
  }

  handleKeyDown(event: any) {

    if (event.keyCode == 13) {

      var wasEmpty= this.newValue == null || typeof this.newValue == 'undefined';
      this.addValue(true);
      if(wasEmpty)
        this.submitted.emit();
    }
  }

  refresh() {
    this.cleared.emit()
  }
  checkWord(val: any) {
    let text = val
    if (val && val.length > 0) {
      if (val.indexOf(' ') > -1) {
        text = text.split(' ')
        const lastWord: string = text[text.length - 1]
        const isCapitalized = lastWord.charAt(0) !== lastWord.charAt(0).toLowerCase()
        const isUppercase = lastWord === lastWord.toUpperCase()
        if (lastWord.toLowerCase().indexOf('oe') > -1) {
          var newText
          newText = this.autoCorrect(lastWord.toLowerCase())
          if (isUppercase) newText = newText.toUpperCase()
          else if (isCapitalized)
            newText = newText.charAt(0).toUpperCase() + newText.slice(1)
          text[text.length - 1] = newText
        }

      }

      else {
        if (text.toLowerCase().indexOf('oe') > -1) {
          const lastWord: string = text[text.length - 1]
          const isCapitalized = lastWord[0].charAt(0) !== lastWord[0].charAt(0).toLowerCase()
          const isUppercase = lastWord === lastWord.toUpperCase()
          var newText
          newText = this.autoCorrect(text.toLowerCase())
          if (isUppercase) newText = newText.toUpperCase()
          else if (isCapitalized)
            newText = newText.charAt(0).toUpperCase() + newText.slice(1)

          text = newText
        }
      }
      if (Array.isArray(text))
        text = text.join(' ')

      val = text
    }

    return val
  }

  autoCorrect(word: any) {
    if (word.length == 1) {
      return word
    }

    let prefix

    // If word has apostrophe like l'oeuf or l'oeil, we only need the word after the apostrophe
    if (word.indexOf("'") > -1) {
      prefix = word.split("'")[0]
      word = word.split("'")[1]
    }

    if (prefix) {
      word = prefix.concat("'", word)
    }

    return word
  }

}
