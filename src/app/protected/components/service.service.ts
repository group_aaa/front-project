import { Injectable } from "@angular/core";
import {HttpClient, HttpHeaders, HttpParams} from "@angular/common/http";
import {LoginResponse} from "../../public/interfaces";
import {catchError, tap} from "rxjs";
import {LOCALSTORAGE_TOKEN_KEY, tokenGetter} from "../../app.module";
import {Product, Report} from "./interfaces";
import { ToastrService } from "ngx-toastr";
import { of, throwError as observableThrowError } from "rxjs";
import {AppErrorToIgnore} from "./shared/errors/app-error-to-ignore";
import {AppError} from "./shared/errors/app-error";
import {Router} from "@angular/router";



@Injectable()
export class ServiceService
{
  constructor(private http: HttpClient, private router: Router, private toastr: ToastrService){}

  initHeaders() {
    let headers = new HttpHeaders();
    headers = headers.append("Access-Control-Allow-Origin", "*");
    headers = headers.append('Content-Type','application/json');

    headers = headers.append('Authorization', 'Bearer '+ tokenGetter())
    return headers;
  }
  getProducts() {
    return this.http.get('http://localhost:8087/product/all', {headers: this.initHeaders()}).pipe(
      catchError(this.handleError())
    );
  }

  getProduct(id: string) {
    return this.http.get('http://localhost:8087/product/'+ id, {headers: this.initHeaders()}).pipe(
      catchError(this.handleError())
    );
  }

  createProduct(product: Product) {
    return this.http.post('http://localhost:8087/product', product, {headers: this.initHeaders()}).pipe(
      catchError(this.handleError())
    );
  }

  updateProduct(product: Product) {
    return this.http.put('http://localhost:8087/product', product, {headers: this.initHeaders()}).pipe(
      catchError(this.handleError())
    );
  }

  generateReport(report: Report) {
    return this.http.post('http://localhost:8090/report/requestReport?reportType='+ report.id,
      report.productIdsSelected,
      {headers: this.initHeaders()}).pipe(
        catchError(this.handleError())
    );
  }

  private handleError() {
    return (error: any) => {
      let errorMessage;
      if (error.status === 511) {
        localStorage.clear();
        this.router.navigate(['/public/login']);
        this.toastr.error(error.error.errorMessage, 'Error');
        return observableThrowError(new AppError(''));
      }
      if (error.status === 0)
        errorMessage = 'Error!';
      else errorMessage = error.error.errorMessage;
      this.toastr.error(errorMessage, 'Error');
      return observableThrowError(new AppErrorToIgnore() as any);
    }
  }
}
