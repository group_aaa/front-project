import { Component, OnInit } from '@angular/core';
import {Category, CategoryList, Country, CountryList, Ingredient, Product} from "../../interfaces";
import {ActivatedRoute} from "@angular/router";
import {ServiceService} from "../../service.service";

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.css']
})
export class ProductComponent implements OnInit {

  product : Product = new Product();
  id: string = '';
  CategoryList: any[] = []
  CountryList: any[] = []
  constructor(private route: ActivatedRoute, private serviceSevice: ServiceService) { }

  ngOnInit(): void {
    this.CategoryList = CategoryList;
    this.CountryList = CountryList;
    this.id = this.route.snapshot.params['id'];
    if (this.id)
    this.serviceSevice.getProduct(this.id).subscribe(res => {
      this.product = res as Product;
    });
  }

  saveProduct() {
    if (this.isValid(this.product)) {
      if (this.product.id)
        this.serviceSevice.updateProduct(this.product).subscribe();
      else this.serviceSevice.createProduct(this.product).subscribe();
    }
  }

  isValid(product: Product) {
    return product.name
      && product.price
      && product.countriesSell.length > 0
      && product.category.name;
  }

  addRow(list: any) {
    list.push();
  }

  addOrRemoveAllergy(list: string[], addOrRemove: number, index?: number) {
    switch (addOrRemove) {
      case 0 :
        list.push('');
        break;
      case 1 :
        if (index) list.splice(index, 1);
        break;
    }
  }

  addOrRemoveIngredient(list: Ingredient[], addOrRemove: number, index?: number) {
    switch (addOrRemove) {
      case 0 :
        list.push(new Ingredient());
        break;
      case 1 :
        if (index) list.splice(index, 1);
        break;
    }
  }

  addOrRemoveCountry(list: Country[], addOrRemove: number, index?: number) {
    switch (addOrRemove) {
      case 0 :
        list.push(new Country());
        break;
      case 1 :
        if (index) list.splice(index, 1);
        break;
    }
  }

  protected readonly Category = Category;
  protected readonly Country = Country;
}
