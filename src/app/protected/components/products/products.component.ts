import { Component, OnInit } from '@angular/core';
import {Product} from "../interfaces";
import {ServiceService} from "../service.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.css']
})
export class ProductsComponent implements OnInit {

  user: any;
  products: Product[] = [];
  constructor(private serviceService: ServiceService,private router: Router) { }

  ngOnInit(): void {
    //this.user = userGetter();

    this.serviceService.getProducts().subscribe(res => {
      this.products = res as Product[]
    });
  }
}
