import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProtectedRoutingModule } from './protected-routing.module';
import { MatButtonModule } from '@angular/material/button';
import {HomeComponent} from "./components/home/home.component";
import { ProductsComponent } from './components/products/products.component';
import { ReportsComponent } from './components/reports/reports.component';
import {ServiceService} from "./components/service.service";
import { ProductComponent } from './components/products/product/product.component';
import {FormsModule} from "@angular/forms";
import { MultiSelectFieldComponent } from './components/shared/multi-select-field/multi-select-field.component';
import { MultiValueFieldComponent } from './components/shared/multi-value-field/multi-value-field.component';
import {ToastrModule} from "ngx-toastr";

@NgModule({
  declarations: [
    HomeComponent,
    ProductsComponent,
    ReportsComponent,
    ProductComponent,
    MultiSelectFieldComponent,
    MultiValueFieldComponent
  ],
  providers: [
    ServiceService
  ],
  imports: [
    CommonModule,
    ProtectedRoutingModule,
    MatButtonModule,
    FormsModule,
    ToastrModule.forRoot()
  ]
})
export class ProtectedModule { }
