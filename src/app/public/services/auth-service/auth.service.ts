import {LOCALSTORAGE_SUPPLIER_ID, LOCALSTORAGE_TOKEN_KEY} from './../../../app.module';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import { Injectable } from '@angular/core';
import {catchError, map, Observable, of, switchMap, tap} from 'rxjs';
import { JwtHelperService } from '@auth0/angular-jwt';
import { MatSnackBar } from '@angular/material/snack-bar';
import { LoginRequest, LoginResponse } from '../../interfaces';
import {Router} from "@angular/router";
import {ToastrService} from "ngx-toastr";
import {AppError} from "../../../protected/components/shared/errors/app-error";
import {AppErrorToIgnore} from "../../../protected/components/shared/errors/app-error-to-ignore";
import { throwError as observableThrowError } from "rxjs";


@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(
    private http: HttpClient,
    private snackbar: MatSnackBar,
    private jwtService: JwtHelperService,
    private router: Router,
    private toastr: ToastrService
  ) { }

  login(loginRequest: LoginRequest): Observable<LoginResponse> {

    let headers = new HttpHeaders();
    headers.append('Content-Type','application/json');
    headers = headers.append("Access-Control-Allow-Origin", "*");

    return this.http.post<LoginResponse>('http://localhost:8089/auth', loginRequest, {headers : headers})
      .pipe(
        tap((res: LoginResponse) => {
          if(res.result) {
            localStorage.setItem(LOCALSTORAGE_TOKEN_KEY, res.result.accessToken)
            localStorage.setItem(LOCALSTORAGE_SUPPLIER_ID, res.result.supplierId);
          }
        }),
          catchError(this.handleError() as any)
      );
  }

  private handleError() {
    return (error: any) => {
      let errorMessage;
      this.toastr.error("Username or Password is incorrect", 'Error');
      return observableThrowError(new AppErrorToIgnore() as any);
    }
  }

  /*
   Get the user fromt the token payload
   */
  getLoggedInUser() {
    const decodedToken = this.jwtService.decodeToken();
    return decodedToken.user;
  }
}
